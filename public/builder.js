//the function dynamically creates a responsive table holding the results of the las test run
function createResultTable(where, columnsNo, dbEntryJson){
    const resultJson = dbEntryJson.results;
    const logsJson = dbEntryJson.logs;
    const testNames = Object.keys(resultJson);
    const rowTotal = testNames.length;

    const location = document.querySelector(where);
    const responsiveDiv = document.createElement('div');
    responsiveDiv.className = 'table-responsive';
    const table = document.createElement('table');
    table.className = 'table table-hover';

    const header = document.createElement('thead');
    const headerRow = document.createElement('tr');
    const headerTitles = ['Tested area', 'Result', 'Logs location'];
    let columnCounter;
    for (columnCounter = 0; columnCounter < columnsNo; columnCounter++){
      const headerCell = document.createElement('th');
      headerCell.className = 'text-center';
      headerCell.innerHTML = headerTitles[columnCounter];
      headerRow.appendChild(headerCell);
    }
    header.appendChild(headerRow);
    table.appendChild(header);

    const body = document.createElement('tbody');
    let currentTestIndex = 0;
    let rowCounter;
    for (rowCounter = 0; rowCounter < rowTotal; rowCounter++){
      const bodyRow = document.createElement('tr');
      let bodyColumnCounter;
      for (bodyColumnCounter = 0; bodyColumnCounter < columnsNo; bodyColumnCounter++){
        const bodyCell = document.createElement('td');
        const testName = testNames[currentTestIndex];
        switch(bodyColumnCounter){
          case 0:
          bodyCell.innerHTML = testName;
          break;
          case 1:
          bodyCell.innerHTML = resultJson[testName];
          break;
          case 2:
          bodyCell.innerHTML = logsJson[testName];
          break;
        }
        bodyRow.appendChild(bodyCell);
      }
      currentTestIndex++;
      body.appendChild(bodyRow);
    }
    table.appendChild(body);
    responsiveDiv.appendChild(table);
    location.appendChild(responsiveDiv);
}

//the function for communicating with the server via GET
function sendGET(url, cbk){
  const xhr = new XMLHttpRequest();
  xhr.open('get', url);

  xhr.onreadystatechange = function() {
    if (xhr.readyState == 4 && xhr.status == 200) {
      cbk(JSON.parse(xhr.responseText));
    }
  }
  xhr.send();
}

//the function for communicating with the server via GET
function sendGETdata(url, data, cbk) {
  const xhr = new XMLHttpRequest();
  xhr.open('get', url + '?' + data);

  xhr.onreadystatechange = function() {
    if (xhr.readyState == 4 && xhr.status == 200) {
      cbk(JSON.parse(xhr.responseText));
    }
  }
  xhr.send();
}


//the function makes the informaton about test rerun visible and hides it after 6 sec
function displayInfoBlock(){
  const block = document.querySelector('#rerunDiv');
  block.style.display = 'block';
  function make_disappear(){
    block.style.display = 'none';
  }
  window.setTimeout(make_disappear, 6000);
}

//the function creates a button with on-click eventListener - it sends an email that triggers test
function createRerunButton(where){
  const location = document.querySelector(where);
  rerunButton = document.createElement('a');
  rerunButton.className = 'btn btn-default btn-lg';
  rerunButton.innerHTML = 'Run again!';
  rerunButton.style.opacity = 1;
  rerunButton.addEventListener('click', function(){
    sendGET('/rerun', function(){console.log('Sent!')});
    displayInfoBlock();
  });
  location.appendChild(rerunButton);
}

//the function checks ids of elements which shoudl be filled with charts
function getAllChartContainers(){
	const chartContainerClassElements = document.querySelectorAll('.chartContainer');
	const chartIds = [];
	let elemntCounter;
	for (elementCounter = 0; elementCounter < chartContainerClassElements.length; elementCounter++){
		const id = chartContainerClassElements[elementCounter].getAttribute('id');
		chartIds.push(id);
	}
	return chartIds;
}

//the function appends a close button to a specified div
function addCloseButton(where){
  const location = document.querySelector(where);
  closeButton = document.createElement('a');
  closeButton.className = 'close';
  closeButton.innerHTML = 'x';
  closeButton.addEventListener('click', function(){
    const detailsDisplay = document.querySelector('#detailsDiv');
    detailsDisplay.innerHTML = '';
    detailsDisplay.style.display = 'none';
  });
  location.appendChild(closeButton)
}

//the function makes the details links clickable
function activateDetailsButtons(){
  const detailsLinks = document.querySelectorAll('.details');
  let detailsCounter;
  for (detailsCounter = 0; detailsCounter < detailsLinks.length; detailsCounter++){
    const currentDetailId = detailsLinks[detailsCounter].id;
    detailsLinks[detailsCounter].addEventListener('click', function(){
      const number = this.id .substr(6);
      if (!isNaN(number) && parseInt(number) < 5){
        sendGETdata('/displayDetails', number, function(params){
          const detailsDisplay = document.querySelector('#detailsDiv');
          detailsDisplay.innerHTML = '';
          addCloseButton('#detailsDiv');
          createResultTable('#detailsDiv', 3, params[0]);
          resultsTable = detailsDiv.querySelector('table');
          resultsTable.id = 'table-details'
          detailsDisplay.style.display = 'block';
        });
      }
    }
    );
  }
}

//the function creates a heading with date
function createDatedHeading(where, comparativeJson, type){
  const months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
  const testDate = comparativeJson.date;


  const testYear = testDate.substring(0,4);
  const testMonth = months[parseInt(testDate.substring(5,7)) - 1];
  const testDay = testDate.substring(8,10);

  if (type == 1){
    const location = document.querySelector(where);
    const datedHeading = document.createElement('h3');
    datedHeading.className = 'solitaryHeader';
    datedHeading.innerHTML = `Results of the test run from ${testDay} of ${testMonth} ${testYear}`;
    location.appendChild(datedHeading);
  }
  else if (type == 2){

    const testStatus = comparativeJson.status;
	   function getParagraph(){
    		const searchPhrase = '#'+where;
      	const sameLevelElements = document.querySelector(searchPhrase).parentElement.childNodes;
    		let siblingsCounter;
    		for (siblingsCounter = 0; siblingsCounter < sameLevelElements.length; siblingsCounter++){
    			if (sameLevelElements[siblingsCounter].tagName == 'P'){
    			return sameLevelElements[siblingsCounter];
    		}	}
}
	const datedHeading = getParagraph();
  datedHeading.innerHTML = `${testStatus} on ${testDay} of ${testMonth} ${testYear}`;
}
}

//the function creates charts based on json it receives from db
function visualizePastRun(where, comparativeJson){
	const keys = Object.keys(comparativeJson);
	const undef = undefined;
	if (keys.indexOf('undefined') > -1){
		undef = comparativeJson.undefined;
	}
	const passed = comparativeJson.passed;
	const failed = comparativeJson.failed;
	let chartData;

	if (undef != undefined){
		chartData = {
	    labels: [
	        "Passed",
	        "Failed",
	        "Undefined"
	    ],
	    datasets: [
	        {
	            data: [passed, failed, undef],
	            backgroundColor: [
	                "#5CB85C",
	                "#FF5722",
	                "#008744"
	            ],
	            hoverBackgroundColor: [
	                "#4BA74B",
	                "#EE4611",
	                "#007633"
	            ]
	        }]
				}
			} else {
					chartData = {
						labels: [
								"Passed",
								"Failed",
						],
						datasets: [
								{
										data: [passed, failed],
										backgroundColor: [
												"#5CB85C",
												"#FF5722"
										],
										hoverBackgroundColor: [
												"#4BA74B",
												"#EE4611"
										]
								}]
							}
}
const ctx = document.getElementById(where).getContext('2d');
const chartVisualization = new Chart(ctx, {
	type: 'doughnut',
	data: chartData,
	options: {
        legend: {
            display: true,
            labels: {
                fontColor: '#FFFFFF'
            }
        }
}
});
}

//the function works with db jsons - returns the data needed to create charts (percentages, dates, runID)
function makeComparable(dbEntryJson){
  const results = Object.values(dbEntryJson.results);
  let pass = 0;
  let fail = 0;
  let undef = 0
  let resultCounter = 0;
  for (resultCounter = 0; resultCounter < results.length; resultCounter++){
    if (results[resultCounter].toLowerCase() == 'passed'){
      pass++;
    }
    else if (results[resultCounter].toLowerCase() == 'failed'){
      fail++;
    }
    else {
      undef++;
    }
  }

  const passPercent = (pass/results.length) * 100;
  const failPercent = (fail/results.length) * 100;
  const undefPercent = 0;
  if (undef != 0){
  undefPercent = (undef/results.length) * 100;
}

const comparableObject = new Object();
comparableObject.runID = dbEntryJson.runID;
comparableObject.date = dbEntryJson.date;
comparableObject.passed = passPercent;
comparableObject.failed = failPercent;
if (undefPercent > 0 ){
  comparableObject.undefined = undefPercent;
  }
  return comparableObject;
}

//the function prepares an array of datapoints for the chart-rendering function
function getComparableResultsArray(last5results){
  const statisticalData = [];
  let statisticsCounter;
  for (statisticsCounter = (last5results.length)-1; statisticsCounter >= 0 ; statisticsCounter--){
    const analyzedEntry = last5results[statisticsCounter];
    const dataPoint = makeComparable(analyzedEntry);
    statisticalData.push(dataPoint);
  }
  return statisticalData;
}

//the function compares the tests results to know what to put in the header div
function decideStatus(last5results){
  const comparableArray = getComparableResultsArray(last5results);
  const passReference = comparableArray[(comparableArray.length) - 1].passed;
  let statusCounter;
  for (statusCounter = 0; statusCounter < (comparableArray.length) - 1; statusCounter++){
    currentEntry = comparableArray[statusCounter];
    //console.log(currentEntry.passed);
    if (currentEntry.passed < passReference){
      currentEntry.status = 'Better than';
    } else if (currentEntry.passed > passReference){
      currentEntry.status = 'Worse than';
    } else {
      currentEntry.status = 'Same as';
    }
  }
  return comparableArray;
}

//call the functions from above once you receive response from server
sendGET('/displayResults', function(params){
  createDatedHeading('#resultsDiv', params[0], 1);
  createResultTable('#resultsDiv', 3, params[0]);
  createRerunButton('.table-responsive');
  const jsonsArray = decideStatus(params);
  const ids = getAllChartContainers();
  let containersCount;
  for (containersCount = 0; containersCount < ids.length; containersCount++){
  visualizePastRun(ids[containersCount], jsonsArray[containersCount]);
  //console.log(jsonsArray[containersCount]);
  createDatedHeading(ids[containersCount], jsonsArray[containersCount], 2);
}
});
activateDetailsButtons();

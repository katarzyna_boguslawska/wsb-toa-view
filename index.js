const express = require('express');
const bodyParser = require('body-parser');
const app = express();


const model = require('./app/model');
const sender = require('./app/sender');

app.use(express.static('public'));
app.use(bodyParser.urlencoded({ extended: true }));

//request for database information
app.get('/displayResults', function(req, res){
  model.retrieveDocuments(res);
});

app.get('/displayDetails', function(req, res){
  model.getPastResult(req, res);
});

//request for email
app.get('/rerun', function(req, res){
  sender.rerun(res);
});


//server communication
app.listen(3000, function () {
  console.log('Example app listening on port 3000!');
});

'use strict';
const nodemailer = require('nodemailer');
const settings = require('./settings');
const host = settings.host;

//the function sends an email of pre-defined content and recipent data = to trigger tests
function rerun(cbk){
  const transporter = nodemailer.createTransport({
      host: settings.host,
      port: settings.port,
      secure: true,
      auth: {
          user: settings.user,
          pass: settings.pass
      }
  });

  const mailOptions = {
      from: '"Python WSB" <python.wsb@poczta.fm>',
      to: 'python.wsb@poczta.fm',
      subject: '[TEST.TRIGGER]Test are about to start',
      html: '<b>Your automated tests have started! You will be informed about the results with a separate email. Cheers!</b>'
  };

  transporter.sendMail(mailOptions, function(error, info){
      if(error){
          return console.log(error);
      }
      console.log('Message sent: ' + info.response);
      cbk.send({'message':info.response});
  });
}
exports.rerun = rerun;

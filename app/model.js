const settings = require('./settings');
const MongoClient = require('mongodb').MongoClient,
  f = require('util').format,
  assert = require('assert');

//the function is a wrapper around a db query to build the current results table
function retrieveDocuments(cbk){
    const url = settings.url
    MongoClient.connect(url, function(err, db) {
      assert.equal(null, err);
      console.log("Connected correctly to server");
      const col = db.collection('testTrello');
      col.find().sort({runID:-1}).limit(5).toArray(function(err, docs){
        assert.equal(null, err);
        assert.equal(5, docs.length);
        cbk.send(docs);
        db.close();
      });
    });
}

//the function is a wrapper around a db query to build the details of past results table
function getPastResult(which, cbk){
  const url = settings.url
  const query_string = which.query;
  const document_number = parseInt(Object.keys(query_string)[0]);
  MongoClient.connect(url, function(err, db) {
    assert.equal(null, err);
    console.log("Connected correctly to server");
    const col = db.collection('testTrello');
    col.find().sort({runID:-1}).skip(document_number).limit(1).toArray(function(err, doc){
      assert.equal(null, err);
      assert.equal(1, doc.length);
      cbk.send(doc);
      db.close();
    });
  });
}

exports.retrieveDocuments = retrieveDocuments;
exports.getPastResult = getPastResult;

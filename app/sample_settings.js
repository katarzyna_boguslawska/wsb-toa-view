const host = 'example.com';
const port = 123;
const user = "mailbox_to_query@example.com";
const pass = "password";
const url = 'mongodb://read_uer:read_password@localhost:27017/test_results_db';

exports.host = host;
exports.port = port;
exports.user = user;
exports.pass = pass;
exports.url = url;

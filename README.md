## Synopsis

This is the view part of a test automation project located [here](https://bitbucket.org/katarzyna_boguslawska/wsb-toa-page-object-python-2017). It displays the results of
test execution on a website. It queries a database to obtain these results. It also allows triggering new test runs via the website.

## Motivation

This project is an addition to the core of tests, originating from the observation that unless automated tests results are analyzed, they make absolutely no sense. A web visualization of the results is supposed to facilitate the analysis.

## Installation

Except for the code from this repository, you will need:

* a running [MongoDB](https://www.mongodb.com/download-center#community) instance with a read-only user and a write user
* [NodeJS v4.2.6](https://nodejs.org/en/download/)
* [Express package]( https://expressjs.com)
* [body-parser package](https://github.com/expressjs/body-parser)
* [Node-mailer package v2.7.2](https:nodemailer.com)
* [MongoDB Node driver](https://mongodb.github.io/node-mongodb-native/)
* [Chart.js package](http://www.chartjs.org/docs/)

## Contact

Feel free to contact the repository owner for any information on the project.
